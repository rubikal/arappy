ArAppy::Application.routes.draw do

  root :to => "apps#index"
  devise_for :users
  resources :users
  resources :apps do
  	collection do
  		post :fetch
      post :top_rank
      get :filter
      get :top
  	end
  end

  get '/settings' => 'stats#settings', :as => :settings

  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'
end