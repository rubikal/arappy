class AppSearch
	include ActiveModel::Conversion
  extend ActiveModel::Naming

  attr_accessor :country, :category

  def persisted?
    false
  end
end
