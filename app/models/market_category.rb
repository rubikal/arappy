class MarketCategory
  include Mongoid::Document
  belongs_to :category
  belongs_to :market
  has_and_belongs_to_many :apps
end
