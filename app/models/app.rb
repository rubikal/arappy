class App
  include Mongoid::Document
  include Sidekiq::Worker
  sidekiq_options :backtrace => true

  field :identifier, type: String
  field :name, type: String
  field :title, type: String
  field :link, type: String
  has_and_belongs_to_many :market_categories
  embeds_many :ranks
  has_and_belongs_to_many :markets

  LIMIT = 10

  def rank_at(market_category, rank = nil)
    app_rank = ranks.where(market_category: market_category).first
    if rank
      app_rank.position = rank
      app_rank.save
    end
    app_rank
  end

  class << self
    def in_category(category)
      any_in(market_category_ids: category.market_category_ids)
    end

    def fetch_all
      Market::MARKETS.keys.each do |market_iso|
        market = Market.find_or_create_by iso: market_iso
        market.weight = Market::MARKETS[market_iso][:weight]
        market.save
        Category::VALUES.each do |category_symbol|
          category = Category.find_or_create_by symbol: category_symbol
          market_category = MarketCategory.find_or_create_by category: category, market: market
          apps_feed = HTTParty.get("https://itunes.apple.com/#{market_iso}/rss/#{category_symbol}/limit=#{LIMIT}/json")
          market_type_apps = apps_feed['feed']['entry']
          index = 1
          market_type_apps.each do |application|
            app_id = application['id']['attributes']['im:id']
            app = App.find_or_create_by identifier: app_id
            app.name = application['im:name']['label']
            app.title = application['title']['label']
            link = nil
            if application['link'].instance_of? Array
              link = application['link'][0]['attributes']['href']
            else
              link = application['link']['attributes']['href']
            end
            app.link = link.gsub /#{market_iso}\/|\?.*/, '' 
            app.markets << market unless app.markets.include? market
            app.market_categories << market_category unless app.market_categories.include? market_category
            if app.ranks.where(market_category: market_category).exists?
              app_rank = app.ranks.where(market_category: market_category).first
              app_rank.position = index
            else
              app.ranks.create position: index, market_category: market_category
            end

            app.save
            index += 1
          end
        end
      end
    end
  end
  
end
