class Market
  include Mongoid::Document
  field :iso, type: String
  field :weight, type: Float
  has_and_belongs_to_many :apps
  has_many :market_categories
  MARKETS = {
  	:eg => {:weight => 8.25},
  	:bh => {:weight => 1.75},
  	:dz => {:weight => 3.00},
  	:il => {:weight => 4.00},
  	:jo => {:weight => 3.50},
  	:kw => {:weight => 3.25},
  	:lb => {:weight => 3.00},
  	:om => {:weight => 1.75},
  	:qa => {:weight => 3.00},
  	:sa => {:weight => 11.00},
  	:tn => {:weight => 2.25},
  	:tr => {:weight => 1.25},
  	:ae => {:weight => 5.50},
  	:ye => {:weight => 2.00}
  }

  class << self
  	def method_missing(name, *args, &block)
      if name =~ /^#{MARKETS.keys.join('|')}$/
        Market.where(iso: name).first
      else
        super
      end
    end
  end
end
