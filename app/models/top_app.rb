class TopApp
  include Mongoid::Document
  embedded_in :category
  belongs_to :app

  field :order, type: Integer
  field :aggregated_rank, type: Float
end
