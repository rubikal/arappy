class Rank
  include Mongoid::Document
  field :position, type: Integer

  belongs_to :market_category
  embedded_in :app

end
