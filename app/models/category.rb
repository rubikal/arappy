class Category
  include Mongoid::Document
  include Sidekiq::Worker
  sidekiq_options :backtrace => true
  
  has_many :market_categories
  embeds_many :top_apps

  field :symbol, type: String

  VALUES = [:topfreeapplications, :toppaidapplications, :topgrossingapplications, :topfreeipadapplications, :toppaidipadapplications, :topgrossingipadapplications, :newapplications, :newfreeapplications, :newpaidapplications]

  def apps
    market_categories.collect(&:apps)
  end

  class << self
    def rank_top_apps
      Category.each do |category|
        category_top_apps = []
        category_apps = App.in_category(category)
        category_apps.each do |app|
          aggregated_rank = 0
          app.market_categories.where(category: category).each do |market_category|
            market = market_category.market
            app_rank = app.rank_at market_category
            aggregated_rank += market.weight / app_rank.position
          end
          category_top_apps << [app, aggregated_rank]
        end
        category_top_apps.sort! {|a, b| b[1] <=> a[1]}
        category.top_apps.delete_all
        category_top_apps[0...50].each_with_index do |category_top_app, i|
          category.top_apps.create app: category_top_app[0], aggregated_rank: category_top_app[1], order: (i+1)
        end
      end
    end
  end
end
