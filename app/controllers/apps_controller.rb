class AppsController < ApplicationController

	def filter
    @markets = Market.all
    @apps = []
    countries = params[:market].try(:keys) || []
    types = params[:type].try(:keys) || []
    categories = params[:category].try(:keys) || []
    mixed_categories = []
    categories.each do |category|
      types.each do |type|
        mixed_categories << "top#{category}#{'ipad' if type == 'ipad'}applications"
      end
    end

    countries.each do |country|
      market = Market.where(iso: country).first
      mixed_categories.each do |category_symbol|
        category = Category.where(symbol: category_symbol).first
        market_category = market.market_categories.where(category: category).first
        @apps.concat market_category.apps
      end
    end

    @apps = Kaminari.paginate_array(@apps).page(params[:page])
	end

  def top
    @apps = []
    type = params[:type]
    category = params[:category]
    top = params[:top]
    category_symbol = "top#{category}#{'ipad' if type == 'ipad'}applications"
    category = Category.where(symbol: category_symbol).first

    if type && category
      @apps = category.top_apps.limit(top).collect(&:app)
      @apps = Kaminari.paginate_array(@apps).page(params[:page])
    end
  end

  def index
	  @apps = App.page(params[:page])
  end

  def fetch
  	fetch_job = App.delay.fetch_all
    Sidekiq.redis do |conn|
      conn.set 'app_fetching_job', fetch_job
    end
  	redirect_to settings_path
  end

  def top_rank
    rank_job = Category.delay.rank_top_apps
    Sidekiq.redis do |conn|
      conn.set 'app_ranking_job', rank_job
    end
    redirect_to settings_path
  end

  def destroy
    if App.where(id: params[:id]).exists?
      app = App.find(params[:id])
      app.delete
      flash[:info] = "Application '#{app.name}' has been removed!"
      redirect_to request.referer
    else
      flash[:info] = "Application was not found!"
      redirect_to request.referer
    end
  end
end
