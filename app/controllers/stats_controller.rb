class StatsController < ApplicationController

  def settings
  	@app_fetching_job = nil
  	@app_ranking_job = nil
  	Sidekiq.redis do |conn|
  		@app_fetching_job = conn.get 'app_fetching_job'
      @app_ranking_job = conn.get 'app_ranking_job'
    end
  	app_fetching_status = Workiq.status(@app_fetching_job)
  	app_ranking_status = Workiq.status(@app_ranking_job)
  	@app_fetching = app_fetching_status == :working
  	@app_ranking = app_ranking_status == :working
  end

end
